package com.junson.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.junson.Interceptor.LanguageInterceptor;

@Configuration
public class LanguageConfig extends WebMvcConfigurerAdapter {
	
	@Autowired
	private LanguageInterceptor languageInterceptor;
	
    @Override  
    public void addInterceptors(InterceptorRegistry registry) {  
        registry.addInterceptor(languageInterceptor);  
    }  
	
	@Bean
    public ResourceBundleMessageSource messageSource(){
          ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
          messageSource.setUseCodeAsDefaultMessage(true);
          messageSource.setFallbackToSystemLocale(false);
          messageSource.setBasenames("i18n/messages");
          messageSource.setDefaultEncoding("UTF-8");
//          messageSource.setCacheSeconds(2);
          return messageSource;
   }
	
	@Bean(name="localeResolver")
	public LocaleResolver localeResolverBean() {
	    return languageInterceptor.sessionLocaleResolver();
	}
	
	/*
	 * @RequestParam set language(/index.html?lang=cn)
	 * @Bean  
    public LocaleChangeInterceptor localeChangeInterceptor() {  
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();  
        lci.setParamName("lang");  
        return lci;  
    }  */


}
