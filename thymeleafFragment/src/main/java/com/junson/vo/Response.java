package com.junson.vo;

import java.util.HashMap;
import java.util.Map;

public class Response {

	private String code;
	private String info;
	private Map<String, Object> data = new HashMap<String, Object>();
	
	public static Response success() {
		Response msg = new Response();
		msg.setCode("200");
		msg.setInfo("处理成功！");
		return msg;
	}
	public static Response fail() {
		Response msg = new Response();
		msg.setCode("400");
		msg.setInfo("处理失败！");
		return msg;
	}
	
	public Response addData(String key, Object value) {
		this.getData().put(key, value);
		return this;
	}
	
	@Override
	public String toString() {
		return "Msg [code=" + code + ", info=" + info + ", data=" + data + "]";
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
}
