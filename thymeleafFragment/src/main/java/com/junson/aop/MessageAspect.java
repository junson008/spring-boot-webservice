package com.junson.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Aspect
@Service
public class MessageAspect {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@Before("execution(* com.junson.controller..*(..)) && @annotation(messageAnnotation)")
	public void before(JoinPoint joinPoint, MessageAnnotation messageAnnotation) {
		logger.info("@Before==>{}" + messageAnnotation.value());
		logger.info("@Before==>{}" + joinPoint.getSignature().getName());
	}
	
	@AfterReturning("execution(* com.junson.controller..*(..)) && @annotation(messageAnnotation)")
	public void afterReturning(JoinPoint joinPoint, MessageAnnotation messageAnnotation) {
		logger.info("@AfterReturning==>{}" + messageAnnotation.value());
		logger.info("@AfterReturning==>{}" + joinPoint.getSignature().toString());
	}
	
	@AfterThrowing(pointcut = "execution(* com.junson.controller..*(..)) && @annotation(messageAnnotation)", throwing = "ex")
	public void afterThrowing(JoinPoint joinPoint, MessageAnnotation messageAnnotation, Exception ex) {
		logger.info("@AfterThrowing==>{}" + messageAnnotation.value());
		logger.info("@AfterThrowing==>{}" + joinPoint.getSignature().toString());
		logger.info("@AfterThrowing==>{}" + ex);
	}

}
