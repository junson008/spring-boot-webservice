package com.junson.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.junson.aop.MessageAnnotation;
import com.junson.enums.MessageType;
import com.junson.vo.Response;

@RestController
public class TestController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@MessageAnnotation(MessageType.FAIL)
	@RequestMapping("/test")
	public Response t() {
		logger.info("/test");
		try {
			int i = 1/0;
		} catch (Exception e) {
			
			logger.error("异常", e);
		}
		return Response.success();
	}
}
