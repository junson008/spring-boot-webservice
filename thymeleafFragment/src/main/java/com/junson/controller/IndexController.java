package com.junson.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ServerProperties.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value={"/{lang}/index.html", "/index.html"})
	public String index(@PathVariable(value="lang", required=false) String lang,@RequestParam(value="name", required=false, defaultValue="Junson") String name, Model model) {
		model.addAttribute("name", name);
		
		/*Locale locale = LocaleContextHolder.getLocale();
		System.out.println("==>" + messageSource.getMessage("welcome", null, locale));
		model.addAttribute("welcome",messageSource.getMessage("welcome", null, locale));*/
		logger.info("==> lang [{}]", lang);
		return "index.html";
	}
	
	@RequestMapping(value={"/{lang}/list-content.html", "/list-content.html"})
	public String listcontent(@PathVariable(value="lang", required=false) String lang,@RequestParam(value="name", required=false, defaultValue="Junson") String name, Model model) {
		model.addAttribute("name", name);
		
		/*Locale locale = LocaleContextHolder.getLocale();
		System.out.println("==>" + messageSource.getMessage("welcome", null, locale));
		model.addAttribute("welcome",messageSource.getMessage("welcome", null, locale));*/
		logger.info("==> lang [{}]", lang);
		return "list-content.html";
	}
	
	@RequestMapping(value={"/{lang}/content.html", "/content.html"})
	public String content(@PathVariable(value="lang", required=false) String lang, @RequestParam(value="name", required=false, defaultValue="Shareli") String name, Model model) {
		model.addAttribute("name", name);
		return "contact.html";
	}
	
	@RequestMapping("/layout.html")
	public String layout() {
		return "layout/base.html";
	}
}
