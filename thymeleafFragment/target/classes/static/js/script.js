$(function () {
    var ctx = document.getElementById('canvas').getContext('2d');
    new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels : ["周一","周二","周三","周四","周五","周六","周末"],
            datasets : [
                {
                    //统计表的背景颜色
                    labels: ['Item 1'],
                    backgroundColor : "rgba(0,0,255,0.5)",
                    borderColor : "rgba(0,0,255,0.5)",
                    data : [300,555,655,714,899,905,1000]
                },{
                    labels: ['Item 2'],
                    backgroundColor : "rgba(0,255,0,0.5)",
                    borderColor : "rgba(0,255,0,0.5)",
                    data : [314,455,755,814,999,905,1000]
                }
                ,{
                    labels: ['Item 3'],
                    backgroundColor : "rgba(255,0,0,0.5)",
                    borderColor : "rgba(255,0,0,0.5)",
                    data : [114,255,455,414,599,605,500]
                }
            ]
        },

        // Configuration options go here
        options: {

        }
    });
});
