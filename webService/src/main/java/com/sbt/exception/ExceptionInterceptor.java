package com.sbt.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;

@Component
public class ExceptionInterceptor implements EndpointInterceptor{
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
		logger.info("==>handleRequest messageContext[{}]", messageContext);
		logger.info("==>handleRequest endpoint[{}]", endpoint);
		
		return true;
	}

	@Override
	public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
		logger.info("==>handleResponse messageContext[{}]", messageContext);
		return false;
	}

	@Override
	public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
		logger.info("==>handleFault messageContext[{}]", messageContext);
		return false;
	}

	@Override
	public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {
		logger.info("==>afterCompletion messageContext[{}]", messageContext);
	}
	
	
	/*@Autowired 
	private CountryRepository countryRepository; 
	
	@ResponseBody
	@ExceptionHandler(value=Exception.class)
	public ResultResponse exception(Exception exception) {
		System.out.println("==>exception [" + exception.getMessage() + "]");
		ResultResponse response = new ResultResponse();
		 response.setCountry(countryRepository.findCountry("400"));
		return response;
	}
	
	@ResponseBody
	@ExceptionHandler(value=NoHandlerFoundException.class)
	public ResultResponse exception2(Exception exception) {
		System.out.println("==>exception2 [" + exception.getMessage() + "]");
		ResultResponse response = new ResultResponse();
		response.setCountry(countryRepository.findCountry("400"));
		return response;
	}*/
}
