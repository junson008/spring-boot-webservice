/*package com.sbt.exception;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Aspect
@Service
public class ExceptionAOP {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@AfterThrowing(pointcut = "execution(* org.springframework.ws.transport.http.MessageDispatcherServlet.*.*(..))", throwing = "ex")
	public void afterThrowing(JoinPoint joinPoint, Exception ex) {
		logger.info("==>org.springframework.ws.transport.http.MessageDispatcherServlet" + joinPoint.getSignature().toString());
		logger.info("==>" + ex);
	}
	
	@Before("execution(* org.springframework.ws.transport.http.*.*(..))")
	public void before(JoinPoint joinPoint) {
		logger.info("==>" + joinPoint.getSignature().toString());
	}

}
*/