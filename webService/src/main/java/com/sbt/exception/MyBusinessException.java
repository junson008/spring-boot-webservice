package com.sbt.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.transport.http.ClientHttpRequestMessageSender;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Component
public class MyBusinessException extends WebServiceGatewaySupport{
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public MyBusinessException () {
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10000);
        requestFactory.setReadTimeout(10000);
        setMessageSender(new ClientHttpRequestMessageSender(requestFactory));
		
	}

}
